import scala.collection.mutable.ArrayBuffer
import scala.concurrent.Future
import scala.concurrent.duration.DurationInt
import scala.util.Failure
import scala.util.Random
import scala.util.Success
import java.security._
import com.typesafe.config.ConfigFactory
import java.util.concurrent.ConcurrentHashMap
import akka.actor.Actor
import akka.actor.ActorRef
import akka.actor.ActorSystem
import akka.actor.Cancellable
import akka.actor.Props
import akka.actor.Terminated
import akka.actor.actorRef2Scala
import akka.event.LoggingReceive
import spray.client.pipelining.WithTransformerConcatenation
import spray.client.pipelining.sendReceive
import spray.client.pipelining.sendReceive$default$3
import spray.client.pipelining.unmarshal
import spray.http.ContentTypes
import spray.http.HttpEntity
import spray.http.HttpMethods.GET
import spray.http.HttpMethods.POST
import spray.http.HttpRequest
import spray.http.HttpResponse
import spray.http.Uri.apply
import spray.http._
import java.nio.charset.Charset
import spray.json.pimpAny
import spray.json.pimpString
import java.io.{Closeable, File, FileInputStream, FileOutputStream, InputStream, OutputStream}
import java.io.{BufferedInputStream, BufferedOutputStream, ByteArrayOutputStream, InputStreamReader, OutputStreamWriter}
import java.io.{BufferedReader, BufferedWriter, FileReader, FileWriter, Reader, Writer}

object FacebookClient  extends JsonFormats {

  class UserProfile(input_id: String, input_name: String, input_gender: String, input_dob: String, input_relStatus: String) extends java.io.Serializable {
    var id: String = input_id
    var name: String  = input_name
    var gender: String = input_gender
    var dob: String =  input_dob
    var relStatus: String = input_relStatus
  }

  class Photo(input_caption: String, input_url: String, input_tags: ArrayBuffer[String], input_from: String) extends java.io.Serializable {
    var caption: String = input_caption
    var url: String = input_url
    var tags: ArrayBuffer[String] = input_tags
    var from: String = input_from
  }

  class Album(input_name: String, input_message: String) extends java.io.Serializable {
    var name: String = input_name
    var message: String = input_message
  }

  var ipAddress: String = ""
  var initPort = 8080
  var noOfPorts = 0
  var noOfUsers : Integer= 0
  def main(args: Array[String]) {
      ipAddress = args(0)
      noOfPorts = args(1).toInt
      noOfUsers = args(2).toInt
      // create actor system and a watcher actor.
      val system = ActorSystem("FacebookClients", ConfigFactory.load(ConfigFactory.parseString("""{ "akka" : { "actor" : { "provider" : "akka.remote.RemoteActorRefProvider" }, "remote" : { "enabled-transports" : [ "akka.remote.netty.tcp" ], "netty" : { "tcp" : { "port" : 13000 , "maximum-frame-size" : 12800000b } } } } } """)))
      // creates a watcher Actor.
      val watcher = system.actorOf(Props(new Watcher(noOfUsers)), name = "Watcher")
  }
	
   
  object Watcher {
    case class Terminate(node: ActorRef)
  }

  class Watcher(noOfUsers: Int) extends Actor {
    import context._
    import Watcher._
    // keep track of Client Actors.
    var nodesArr = ArrayBuffer.empty[ActorRef]
    // start running after 10 seconds from currentTime.
    val rnd = new Random
    var absoluteStartTime = System.currentTimeMillis() + (10 * 1000)
    // create given number of clients and initialize.
    for (i <- 0 to noOfUsers - 1) {
      var port = initPort + rnd.nextInt(noOfPorts) * 4
      var node = actorOf(Props(new Client()), name = "" + i)
      // Initialize Clients with info like #Tweets, duration of tweets, start Time, router address. 
      node ! Client.Init(absoluteStartTime, port)
      nodesArr += node
      context.watch(node)
    }
    
    var startTime = System.currentTimeMillis()
    // end of constructor

    // Receive block for the Watcher.
    final def receive = LoggingReceive {
      case Terminated(node) =>
        nodesArr -= node
        // when all actors are down, shutdown the system.
        if (nodesArr.isEmpty) {
	  val finalTime = System.currentTimeMillis()
          println("Final:" + (finalTime - startTime))
          context.system.shutdown
        }
      case _ => println("FAILED HERE")
    }
  }

   object Client {
    case class Init(absoluteTime: Long, port: Int)
    case object GetProfile
    case object CreateProfile
    case object CreatePosts
    case object GetFriends
    case object GetPosts
    case object GetLikes
    case object CreatePostsOnPage
    case object GetPostsonPage
    case object UploadPhoto
    case object GetUploadedPhotos
    case object GetPhotos
    case object LikePhoto
    case object GetPhotoLikes
    case object GetAlbums
    case object CreateAlbum
    case object GetAlbumPhotos
    case object UploadAlbumPhoto
    case object UploadPicture
    case object GetAudience
    case object SaveView
    case object GetServerPublicKey
    case object SaveViewManInMiddle
   }
   
   class Client(implicit system: ActorSystem) extends Actor {
    import context._
    import Client._
        /* Constructor Started */
    //var events = ArrayBuffer.empty[Event]
    var id = self.path.name
    val rand = new Random()
    var cancellable: Cancellable = null
    var ctr = 0
    var endTime: Long = 0
    var port = 0
    var pages : ArrayBuffer[String] = ArrayBuffer.empty
    var pagesInfo : List[List[String]] = null
    var photos: ArrayBuffer[String] = ArrayBuffer.empty
    var uploadedPhotos: ArrayBuffer[String] = ArrayBuffer.empty
    var albums: ArrayBuffer[String] = ArrayBuffer.empty
    var retrievedAlbums: List[String] = null
    var serverPublicKey: PublicKey = null
    var keypair = RSA.generateKey
    var publicKey = RSA.encodePublicKey(keypair.getPublic)
    var aesKeysMap: ConcurrentHashMap[String, String] = new ConcurrentHashMap()
    var ecryptedAudienceList: ArrayBuffer[String] = ArrayBuffer.empty
    var postRawAudience: List[String] = null
    var serverToken:String = null
    var encryptedServerToken:String = null
    /* Constructor Ended */

    def generateTweet(): String = {
      return rand.nextString(rand.nextInt(140))
    }

    final def receive = LoggingReceive {
      case Init(absoluteTime, portNo) =>
        port = portNo
	system.scheduler.scheduleOnce(0 seconds, self, GetServerPublicKey)
	system.scheduler.scheduleOnce(10 seconds, self, CreateProfile)
	system.scheduler.scheduleOnce(20 seconds, self, GetAudience)
	system.scheduler.scheduleOnce(30 seconds, self, GetFriends)
        system.scheduler.scheduleOnce(40 seconds, self, CreatePosts)
        system.scheduler.scheduleOnce(50 seconds, self, GetPosts)
        system.scheduler.scheduleOnce(60 seconds, self, GetLikes)
        system.scheduler.scheduleOnce(70 seconds, self, CreatePostsOnPage)
        system.scheduler.scheduleOnce(80 seconds, self, GetPostsonPage)
	system.scheduler.scheduleOnce(90 seconds, self, UploadPhoto)
        system.scheduler.scheduleOnce(100 seconds, self, GetUploadedPhotos)
        system.scheduler.scheduleOnce(110 seconds, self, GetPhotos)
        system.scheduler.scheduleOnce(120 seconds, self, CreateAlbum)
        system.scheduler.scheduleOnce(130 seconds, self, UploadAlbumPhoto)
        system.scheduler.scheduleOnce(140 seconds, self, GetAlbums)
        system.scheduler.scheduleOnce(150 seconds, self, GetAlbumPhotos)
        system.scheduler.scheduleOnce(160 seconds, self, LikePhoto)
        system.scheduler.scheduleOnce(170 seconds, self, GetPhotoLikes)

      case GetServerPublicKey =>
        val pipeline: HttpRequest => Future[HttpResponse] = sendReceive
	val request = HttpRequest(method = POST, uri = "http://" + ipAddress + ":" + port + "/U"+ id + "/initiate",entity = HttpEntity(ContentTypes.`text/plain`, publicKey))
        val responseFuture: Future[HttpResponse] = pipeline(request)
        responseFuture onComplete {
          case Success(result) =>
              var serverResponse = result.entity.data.asString
	      var arr: Array[String] = serverResponse.split("########")
	      serverPublicKey = RSA.decodePublicKey(arr(0))
	      serverToken = RSA.decrypt(arr(1),keypair.getPrivate)
	      println("Server Token Received : " + serverToken)
              encryptedServerToken = RSA.encrypt(serverToken, serverPublicKey)
          case Failure(error) =>
        }


      case CreateProfile =>
        val pipeline: HttpRequest => Future[HttpResponse] = sendReceive
	var aesKey = RSA.generateAESKey
        aesKeysMap.put("U"+id,aesKey)
	val request = HttpRequest(method = POST, uri = "http://" + ipAddress + ":" + port + "/U"+ id,entity = HttpEntity(ContentTypes.`text/plain`, "U"+id + "$$$$$$$" + encryptedServerToken+ "$$$$$$$" + RSA.encryptWithAESKey((new FacebookServer.UserProfile("U"+id,"Ankit"+id,"M","05/18/1990","Single")).toJson.toString,aesKey)))
        val responseFuture: Future[HttpResponse] = pipeline(request)
        responseFuture onComplete {
          case Success(result) =>
               println("Response for Client: " + id +" on account creation is : "+ result.entity.data.asString)
          case Failure(error) =>
        }

	
     case GetAudience =>
	val pipeline: HttpRequest => Future[HttpResponse] = sendReceive
        val request = HttpRequest(method = GET, uri = "http://" + ipAddress + ":" + port + "/U"+ id+"/audience",entity = HttpEntity(ContentTypes.`text/plain`, "U"+id + "$$$$$$$" + encryptedServerToken))
        val responseFuture: Future[HttpResponse] = pipeline(request)
        responseFuture onComplete {
          case Success(result) =>
               println("Audience received for Client ID : "+ id)
	       var audienceList : List[String]= result.entity.data.asString.parseJson.convertTo[List[String]]
	       postRawAudience = audienceList
	       for(i <- 0 to audienceList.length-1){
			var arr: Array[String] = audienceList(i).split("########")
			ecryptedAudienceList += arr(0) + "########" + RSA.encrypt(aesKeysMap.get("U"+id),RSA.decodePublicKey(arr(1)))
			//println(audienceList(i))
	       }
        	ecryptedAudienceList += "U" + id + "########" + RSA.encrypt(aesKeysMap.get("U"+id),keypair.getPublic)
               system.scheduler.scheduleOnce(0 seconds, self, SaveView)
               system.scheduler.scheduleOnce(0 seconds, self, SaveViewManInMiddle)
	       //println(ecryptedAudienceList.toList.toJson.toString)
          case Failure(error) =>
        }
	

  case SaveViewManInMiddle =>
        val pipeline: HttpRequest => Future[HttpResponse] = sendReceive
	val request = HttpRequest(method = POST, uri = "http://" + ipAddress + ":" + port + "/U"+ id + "/saveView",entity = HttpEntity(ContentTypes.`text/plain`, "U"+(id.toInt + 2)%noOfUsers + "$$$$$$$" + encryptedServerToken+ "$$$$$$$" + ecryptedAudienceList.toList.toJson.toString))
        val responseFuture: Future[HttpResponse] = pipeline(request)
        responseFuture onComplete {
          case Success(result) =>
               println("Response for Client: " + id +" on saveView is : "+ result.entity.data.asString)
          case Failure(error) =>
        }

  case SaveView =>
        val pipeline: HttpRequest => Future[HttpResponse] = sendReceive
	val request = HttpRequest(method = POST, uri = "http://" + ipAddress + ":" + port + "/U"+ id + "/saveView",entity = HttpEntity(ContentTypes.`text/plain`, "U"+id + "$$$$$$$" + encryptedServerToken+ "$$$$$$$" + ecryptedAudienceList.toList.toJson.toString))
        val responseFuture: Future[HttpResponse] = pipeline(request)
        responseFuture onComplete {
          case Success(result) =>
               println("Response for Client: " + id +" on saveView is : "+ result.entity.data.asString)
          case Failure(error) =>
        }

 case GetFriends =>
	val pipeline: HttpRequest => Future[HttpResponse] = sendReceive
        val request = HttpRequest(method = GET, uri = "http://" + ipAddress + ":" + port + "/U"+ id+"/friends",entity = HttpEntity(ContentTypes.`text/plain`, "U"+id + "$$$$$$$" + encryptedServerToken))
        val responseFuture: Future[HttpResponse] = pipeline(request)
        responseFuture onComplete {
          case Success(result) =>
               //println("Friend List of User: "+"U"+ id + " is : "+ result.entity.data.asString+"\n")
               var friendsList : List[String]= result.entity.data.asString.parseJson.convertTo[List[String]]
	       var output : String = ""
	       for(i <- 0 to friendsList.length-1){
			var arr: Array[String] = friendsList(i).split("########")
			var decryptedAES = RSA.decrypt(arr(1),keypair.getPrivate)
			var friendList = RSA.decryptWithAESKey(arr(0),decryptedAES)
			output += friendList
			//println(audienceList(i))
	       }
	       println("Friend List of User: "+"U"+ id + " is : " + output)
          case Failure(error) =>
        }


  case CreatePosts =>
	val pipeline: HttpRequest => Future[HttpResponse] = sendReceive
	var tagID: Integer = (id.toInt + 2)%noOfUsers
	var tags =ArrayBuffer[String]("U"+tagID)
	var post: String = (new FacebookServer.Post("","text message","U"+id,"",tags)).toJson.toString
	var aesKey = RSA.generateAESKey
        var encryptedPost: ArrayBuffer[String] = ArrayBuffer.empty
	encryptedPost += RSA.encryptWithAESKey(post, aesKey)
	//Selecting three random friends for viewing the posts including the client itself
	encryptedPost += "U" + id + "########" + RSA.encrypt(aesKey,keypair.getPublic)
	var rand = new Random(System.currentTimeMillis())
	var user = postRawAudience(rand.nextInt(postRawAudience.length))
	var arr: Array[String] = user.split("########")
	encryptedPost += arr(0) + "########" + RSA.encrypt(aesKey,RSA.decodePublicKey(arr(1)))
	user = postRawAudience(rand.nextInt(postRawAudience.length))
	arr = user.split("########")
	encryptedPost += arr(0) + "########" + RSA.encrypt(aesKey,RSA.decodePublicKey(arr(1)))
	user = postRawAudience(rand.nextInt(postRawAudience.length))
	arr = user.split("########")
	encryptedPost += arr(0) + "########" + RSA.encrypt(aesKey,RSA.decodePublicKey(arr(1)))
	//println(encryptedPost.toList.toJson.toString)
        val request = HttpRequest(method = POST, uri = "http://" + ipAddress + ":" + port + "/U"+ id + "/feed",entity = HttpEntity(ContentTypes.`text/plain`, "U"+id + "$$$$$$$" + encryptedServerToken+ "$$$$$$$" + encryptedPost.toList.toJson.toString))
        val responseFuture: Future[HttpResponse] = pipeline(request)
        responseFuture onComplete {
          case Success(result) =>
               println("Post ID for post created by Client: " + id +" is : "+ result.entity.data.asString)
	       aesKeysMap.put(result.entity.data.asString,aesKey)
          case Failure(error) =>
        }

    case GetPosts =>
	val pipeline: HttpRequest => Future[HttpResponse] = sendReceive
        val request = HttpRequest(method = GET, uri = "http://" + ipAddress + ":" + port + "/U"+ id+"/feed",entity = HttpEntity(ContentTypes.`text/plain`, "U"+id + "$$$$$$$" + encryptedServerToken))
        val responseFuture: Future[HttpResponse] = pipeline(request)
        responseFuture onComplete {
          case Success(result) =>
               var postsList : List[String]= result.entity.data.asString.parseJson.convertTo[List[String]]
	       var output : String = ""
	       for(i <- 0 to postsList.length-1){
			var arr: Array[String] = postsList(i).split("########")
			var decryptedAES = RSA.decrypt(arr(1),keypair.getPrivate)
			var post = RSA.decryptWithAESKey(arr(0),decryptedAES)
			println(post)
			output += post

	       }
	       println("Posts shared with clientID U" + id + " are : " + output)
          case Failure(error) =>
        }


    case GetLikes =>
	val pipeline: HttpRequest => Future[HttpResponse] = sendReceive
        val request = HttpRequest(method = GET, uri = "http://" + ipAddress + ":" + port + "/U"+ id+"/likes",entity = HttpEntity(ContentTypes.`text/plain`, "U"+id + "$$$$$$$" + encryptedServerToken))
        val responseFuture: Future[HttpResponse] = pipeline(request)
        responseFuture onComplete {
          case Success(result) =>
               //println("Response for clientID U" +id+ " is : "+ result.entity.data.asString)
	       pagesInfo = result.entity.data.asString.parseJson.convertTo[List[List[String]]]
	       //println("PageIds Retrieved : "+ pages(0) + " & "+ pages(1))
          case Failure(error) =>
        }

   case CreatePostsOnPage =>
       val pipeline: HttpRequest => Future[HttpResponse] = sendReceive
	var tagID: Integer = (id.toInt + 2)%noOfUsers
	var tags =ArrayBuffer[String]("U"+tagID)
	var post: String = (new FacebookServer.Post("","text message","U"+id,"",tags)).toJson.toString
	for(i <- 0 to pagesInfo.length-1){
		var pageDetail: List[String] = pagesInfo(i)
		var pageInfo : String = RSA.decrypt(pageDetail(0), keypair.getPrivate)
		pages += pageInfo
		var aesKey = RSA.generateAESKey
		var encryptedPagePost: ArrayBuffer[String] = ArrayBuffer.empty
		encryptedPagePost += RSA.encryptWithAESKey(post, aesKey)
		for(j <- 1 to pageDetail.length-1){
			var arr = pageDetail(j).split("########")
			encryptedPagePost += arr(0) + "########" + RSA.encrypt(aesKey,RSA.decodePublicKey(arr(1)))
		}
		//println("Post for Client: " + id +" and Page "+ pageInfo +" is : " + encryptedPagePost.toList.toJson.toString)
		val request = HttpRequest(method = POST, uri = "http://" + ipAddress + ":" + port + "/"+ pageInfo + "/feed",entity = HttpEntity(ContentTypes.`text/plain`,"U"+id + "$$$$$$$" + encryptedServerToken+ "$$$$$$$" + encryptedPagePost.toList.toJson.toString))
        val responseFuture: Future[HttpResponse] = pipeline(request)
        responseFuture onComplete {
          case Success(result) =>
               println("Post ID returned for Client: " + id +" and Page "+ pageInfo +" is : "+ result.entity.data.asString )
	       aesKeysMap.put(result.entity.data.asString,aesKey)
          case Failure(error) =>
         }
       }


     case GetPostsonPage =>
	val pipeline: HttpRequest => Future[HttpResponse] = sendReceive
	for(i <- 0 to pages.length-1){
        	val request = HttpRequest(method = GET, uri = "http://" + ipAddress + ":" + port + "/"+ pages(i)+"/feed",entity = HttpEntity(ContentTypes.`text/plain`, "U"+id + "$$$$$$$" + encryptedServerToken ))
	        val responseFuture: Future[HttpResponse] = pipeline(request)
        	responseFuture onComplete {
	          case Success(result) =>
        	       //println("Posts where Page -> " +pages(i)+ " is either tagged/receiver/sender : "+ result.entity.data.asString)
         	       var postsList : List[String]= result.entity.data.asString.parseJson.convertTo[List[String]]
		       var output : String = ""
		       for(i <- 0 to postsList.length-1){
				var arr: Array[String] = postsList(i).split("########")
				var decryptedAES = RSA.decrypt(arr(1),keypair.getPrivate)
				var post = RSA.decryptWithAESKey(arr(0),decryptedAES)
				output += post
		       }
		       println("Posts in Page: "+pages(i) + " is : " + output)
	          case Failure(error) =>
        	}
	}


 case UploadPhoto =>
	var encryptedPicture: ArrayBuffer[String] = ArrayBuffer.empty
	var tagID: Integer = (id.toInt + 2)%noOfUsers
	val arr =ArrayBuffer[String]("U"+tagID)
	var aesKey = RSA.generateAESKey
	var photo: String = (new FacebookClient.Photo("Photo Caption","Photo Link",arr,"U"+id)).toJson.toString
	encryptedPicture += RSA.encryptWithAESKey(photo, aesKey)
	//Selecting three random friends for viewing the posts including the client itself
	encryptedPicture += "U" + id + "########" + RSA.encrypt(aesKey,keypair.getPublic)
	var rand = new Random(System.currentTimeMillis())
	for(i <- 0 to 3){
		var user = postRawAudience(rand.nextInt(postRawAudience.length))
		var arr: Array[String] = user.split("########")
		encryptedPicture += arr(0) + "########" + RSA.encrypt(aesKey,RSA.decodePublicKey(arr(1)))
	}
	val pipeline: HttpRequest => Future[HttpResponse] = sendReceive
        val request = HttpRequest(method = POST, uri = "http://" + ipAddress + ":" + port + "/U"+ id+ "/photos",entity = HttpEntity(ContentTypes.`text/plain`, "U"+id + "$$$$$$$" + encryptedServerToken+ "$$$$$$$" + encryptedPicture.toList.toJson.toString))
        val responseFuture: Future[HttpResponse] = pipeline(request)
        responseFuture onComplete {
          case Success(result) =>
               println("Uploaded picture ID for Client: " + id +"is : "+ result.entity.data.asString )
	       aesKeysMap.put(result.entity.data.asString,aesKey)
	       uploadedPhotos += result.entity.data.asString
          case Failure(error) =>
        }


      case GetUploadedPhotos =>
	val pipeline: HttpRequest => Future[HttpResponse] = sendReceive
	for(i <- 0 to uploadedPhotos.length-1){
        	val request = HttpRequest(method = GET, uri = "http://" + ipAddress + ":" + port + "/"+ uploadedPhotos(i),entity = HttpEntity(ContentTypes.`text/plain`, "U"+id + "$$$$$$$" + encryptedServerToken))
	        val responseFuture: Future[HttpResponse] = pipeline(request)
        	responseFuture onComplete {
	          case Success(result) =>
		       var photosList : List[String]= result.entity.data.asString.parseJson.convertTo[List[String]]
		       var output : String = ""
	   	       for(i <- 0 to photosList.length-1){
				var arr: Array[String] = photosList(i).split("########")
				var decryptedAES = RSA.decrypt(arr(1),keypair.getPrivate)
				var photo = RSA.decryptWithAESKey(arr(0),decryptedAES)
				output += photo
		       }
		       println("Client ID " +id + " has Photo ID " +uploadedPhotos(i)+ " as : "+ output)
	          case Failure(error) =>
        	}
	}


      case GetPhotos =>
	val pipeline: HttpRequest => Future[HttpResponse] = sendReceive
       	val request = HttpRequest(method = GET, uri = "http://" + ipAddress + ":" + port + "/U"+ id + "/photos",entity = HttpEntity(ContentTypes.`text/plain`, "U"+id + "$$$$$$$" + encryptedServerToken))
	val responseFuture: Future[HttpResponse] = pipeline(request)
        	responseFuture onComplete {
	          case Success(result) =>
		       var photosList : List[String]= result.entity.data.asString.parseJson.convertTo[List[String]]
		       var output : String = ""
	   	       for(i <- 0 to photosList.length-1){
				var arr: Array[String] = photosList(i).split("########")
				var decryptedAES = RSA.decrypt(arr(1),keypair.getPrivate)
				var photo = RSA.decryptWithAESKey(arr(0),decryptedAES)
				output += photo
		       }
			println("Photos retrieved for User U" +id + " is "+output )
	          case Failure(error) =>
        	}



    case CreateAlbum =>
	var encryptedAlbum: ArrayBuffer[String] = ArrayBuffer.empty
	var tagID: Integer = (id.toInt + 2)%noOfUsers
	val arr =ArrayBuffer[String]("U"+tagID)
	var aesKey = RSA.generateAESKey
	var album: String = (new FacebookClient.Album("Album Name","Album description")).toJson.toString
	encryptedAlbum += RSA.encryptWithAESKey(album, aesKey)
	val pipeline: HttpRequest => Future[HttpResponse] = sendReceive
        val request = HttpRequest(method = POST, uri = "http://" + ipAddress + ":" + port + "/U"+ id+ "/albums",entity = HttpEntity(ContentTypes.`text/plain`, "U"+id + "$$$$$$$" + encryptedServerToken+ "$$$$$$$" + encryptedAlbum.toList.toJson.toString))
        val responseFuture: Future[HttpResponse] = pipeline(request)
        responseFuture onComplete {
          case Success(result) =>
               println("Album ID of the album created for Client: " + id +"is : "+ result.entity.data.asString)
		albums += result.entity.data.asString
	       aesKeysMap.put(result.entity.data.asString,aesKey)
          case Failure(error) =>
        }

    case UploadAlbumPhoto =>
	var encryptedPicture: ArrayBuffer[String] = ArrayBuffer.empty
	var tagID: Integer = (id.toInt + 2)%noOfUsers
	val arr =ArrayBuffer[String]("U"+tagID)
	var aesKey = RSA.generateAESKey
	var photo: String = (new FacebookClient.Photo("Photo Caption","Photo Link",arr,"U"+id)).toJson.toString
	encryptedPicture += RSA.encryptWithAESKey(photo, aesKey)
	//Selecting three random friends for viewing the posts including the client itself
	encryptedPicture += "U" + id + "########" + RSA.encrypt(aesKey,keypair.getPublic)
	var rand = new Random(System.currentTimeMillis())
	for(i <- 0 to 3){
		var user = postRawAudience(rand.nextInt(postRawAudience.length))
		var arr: Array[String] = user.split("########")
		encryptedPicture += arr(0) + "########" + RSA.encrypt(aesKey,RSA.decodePublicKey(arr(1)))
	}
     val pipeline: HttpRequest => Future[HttpResponse] = sendReceive
     for(i <- 0 to albums.length-1){
        val request = HttpRequest(method = POST, uri = "http://" + ipAddress + ":" + port + "/"+ albums(i) + "/photos",entity = HttpEntity(ContentTypes.`text/plain`, "U"+id + "$$$$$$$" + encryptedServerToken+ "$$$$$$$" + encryptedPicture.toList.toJson.toString))
        val responseFuture: Future[HttpResponse] = pipeline(request)
        responseFuture onComplete {
          case Success(result) =>
               println("Photo ID of the photo uploaded in Album -> " + albums(i) +" is : "+ result.entity.data.asString )
          case Failure(error) =>
        }
      }


   case GetAlbums =>
	val pipeline: HttpRequest => Future[HttpResponse] = sendReceive
       	val request = HttpRequest(method = GET, uri = "http://" + ipAddress + ":" + port + "/U"+ id + "/albums",entity = HttpEntity(ContentTypes.`text/plain`, "U"+id + "$$$$$$$" + encryptedServerToken))
	val responseFuture: Future[HttpResponse] = pipeline(request)
        	responseFuture onComplete {
	          case Success(result) =>
		       retrievedAlbums = result.entity.data.asString.parseJson.convertTo[List[String]]
    		       println("Albums retrieved for User U" +id + " is "+retrievedAlbums )
	          case Failure(error) =>
        	}

     case GetAlbumPhotos =>
	val pipeline: HttpRequest => Future[HttpResponse] = sendReceive
	for(i <- 0 to retrievedAlbums.length-1){
        	val request = HttpRequest(method = GET, uri = "http://" + ipAddress + ":" + port + "/"+ retrievedAlbums(i) + "/photos", entity = HttpEntity(ContentTypes.`text/plain`, "U"+id + "$$$$$$$" + encryptedServerToken))
	        val responseFuture: Future[HttpResponse] = pipeline(request)
        	responseFuture onComplete {
	          case Success(result) =>
 		       var photosList : List[String]= result.entity.data.asString.parseJson.convertTo[List[String]]
		       var output : String = ""
	   	       for(i <- 0 to photosList.length-1){
				var arr: Array[String] = photosList(i).split("########")
				var decryptedAES = RSA.decrypt(arr(1),keypair.getPrivate)
				var photo = RSA.decryptWithAESKey(arr(0),decryptedAES)
				output += photo
		       }
        	       println("Photos of Album " + albums(i) + " are : "+ output)
	          case Failure(error) =>
        	}
	  }

       case LikePhoto =>
        val pipeline: HttpRequest => Future[HttpResponse] = sendReceive
	var tagID: Integer = (id.toInt + 2)%noOfUsers
	val arr =ArrayBuffer[String]("U"+tagID)
	for(i <- 0 to uploadedPhotos.length-1){
	  val request = HttpRequest(method = POST, uri = "http://" + ipAddress + ":" + port + "/"+ uploadedPhotos(i) + "/likes",entity = HttpEntity(ContentTypes.`text/plain`, "U"+id + "$$$$$$$" + encryptedServerToken))
       	  val responseFuture: Future[HttpResponse] = pipeline(request)
        responseFuture onComplete {
          case Success(result) =>
               println("Client: " + id +" liked photo "+ uploadedPhotos(i) +" and response is "+ result.entity.data.asString )
          case Failure(error) =>
         }
       }
	
     case GetPhotoLikes =>
	val pipeline: HttpRequest => Future[HttpResponse] = sendReceive
	for(i <- 0 to uploadedPhotos.length-1){
        	val request = HttpRequest(method = GET, uri = "http://" + ipAddress + ":" + port + "/"+ uploadedPhotos(i)+"/likes",entity = HttpEntity(ContentTypes.`text/plain`, "U"+id + "$$$$$$$" + encryptedServerToken))
	        val responseFuture: Future[HttpResponse] = pipeline(request)
        	responseFuture onComplete {
	          case Success(result) =>
        	       println("Likes of " + uploadedPhotos(i) + " are : "+ result.entity.data.asString)
	          case Failure(error) =>
        	}
	}
     }
  }
}
