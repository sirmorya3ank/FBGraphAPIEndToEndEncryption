import scala.collection.mutable.ArrayBuffer

import spray.json.DefaultJsonProtocol
import spray.json.DeserializationException
import spray.json.JsArray
import spray.json.JsNumber
import spray.json.JsObject
import spray.json.JsString
import spray.json.JsValue
import spray.json.JsonFormat
import spray.json.pimpAny

trait JsonFormats extends DefaultJsonProtocol {
	
	case class UserProfile(id: String, name: String, gender: String, dob: String, relStatus: String)
  case class Post(message: String, sender_id: String, tags: ArrayBuffer[String])

	//implicit val userProfileFormat = jsonFormat5(UserProfile)

	implicit object UserProfileJsonFormat extends JsonFormat[FacebookServer.UserProfile] {
    	
    	def write(profile: FacebookServer.UserProfile) = JsObject(
      		"id" -> JsString(profile.id),
      		"name" -> JsString(profile.name),
      		"gender" -> JsString(profile.gender),
      		"dob" -> JsString(profile.dob),
      		"relStatus" -> JsString(profile.relStatus))

    	def read(value: JsValue) = {
      		value.asJsObject.getFields("id", "name", "gender", "dob", "relStatus") match {
        		case Seq(JsString(id), JsString(name), JsString(gender), JsString(dob), JsString(relStatus)) =>
        			new FacebookServer.UserProfile(id, name, gender, dob, relStatus)
        		case _ => 
        			throw new DeserializationException("Incomplete profile")
      }
    }
  }

  implicit object PostJsonFormat extends JsonFormat[FacebookServer.Post] {
    
    def write(post: FacebookServer.Post) = JsObject(
      "id" -> JsString(post.id),
      "message" -> JsString(post.message),
      "from" -> JsString(post.from),
      "to" -> JsString(post.to),
      "tags" -> JsArray(post.tags.map(_.toJson).toVector))

    def read(value: JsValue) = {
      value.asJsObject.getFields("id", "message", "from", "to", "tags") match {
        case Seq(JsString(id), JsString(message), JsString(from), JsString(to), JsArray(tags)) =>
          new FacebookServer.Post(id, message, from, to, tags.map(_.convertTo[String]).to[ArrayBuffer])
        case Seq(JsString(message), JsString(from), JsArray(tags)) =>
          new FacebookServer.Post(null, message, from, null, tags.map(_.convertTo[String]).to[ArrayBuffer])
        case _ =>
          throw new DeserializationException("Invalid post")
      }
    }
  }

  /*implicit object PhotoCreation2JsonFormat extends JsonFormat[FacebookClient.Photo2] {
    def write(photo: FacebookClient.Photo2) = JsObject(
      "source" -> JsObject(photo.source),
      "caption" -> JsString(photo.caption))

    def read(value: JsValue) = {
      value.asJsObject.getFields("source", "caption") match {
        case Seq(JsObject(source), JsString(caption)) =>
          new FacebookClient.Photo2(source, caption)
        case _ => 
          throw new DeserializationException("Invalid photo")
      }
      
    }
  }*/

  implicit object PhotoCreationJsonFormat extends JsonFormat[FacebookClient.Photo] {
    
    def write(photo: FacebookClient.Photo) = JsObject(
      "caption" -> JsString(photo.caption),
      "url" -> JsString(photo.url),
      "tags" -> JsArray(photo.tags.map(_.toJson).toVector),
      "from" -> JsString(photo.from))

    def read(value: JsValue) = {
      value.asJsObject.getFields("caption", "url", "tags", "from") match {
        case Seq(JsString(caption), JsString(url), JsArray(tags), JsString(from)) =>
          new FacebookClient.Photo(caption, url, tags.map(_.convertTo[String]).to[ArrayBuffer], from)
        case _ =>
          throw new DeserializationException("Invalid photo")
      }
    }
  }

  implicit object PhotoJsonFormat extends JsonFormat[FacebookServer.Photo] {

    def write(photo: FacebookServer.Photo) = JsObject(
      "id" -> JsString(photo.id),
      "album" -> JsString(photo.album),
      "creation_time" -> JsString(photo.creationTime),
      "from" -> JsString(photo.from),
      "name" -> JsString(photo.name),
      "link" -> JsString(photo.link))

    def read(value: JsValue) = {
      value.asJsObject.getFields("id", "album", "creation_time", "from", "name", "link") match {
        case Seq(JsString(id), JsString(album), JsString(creation_time), JsString(from), JsString(name), JsString(link)) =>
          new FacebookServer.Photo(id, album, creation_time, from, name, link)
        case _ =>
          throw new DeserializationException("Invalid photo object")
      }
    }
  }

  implicit object AlbumCreationJsonFormat extends JsonFormat[FacebookClient.Album] {
    
    def write(album: FacebookClient.Album) = JsObject(
      "name" -> JsString(album.name),
      "message" -> JsString(album.message))

    def read(value: JsValue) = {
      value.asJsObject.getFields("name", "message") match {
        case Seq(JsString(name), JsString(message)) =>
          new FacebookClient.Album(name, message)
        case _ =>
          throw new DeserializationException("Invalid album")
      }
    }
  }

  implicit object AlbumJsonFormat extends JsonFormat[FacebookServer.Album] {

    def write(album: FacebookServer.Album) = JsObject(
      "id" -> JsString(album.id),
      "count" -> JsNumber(album.count),
      "name" -> JsString(album.name),
      "description" -> JsString(album.description),
      "creation_time" -> JsString(album.creation_time))

    def read(value: JsValue) = {
      value.asJsObject.getFields("id", "count", "name", "description", "creation_time") match {
        case Seq(JsString(id), JsNumber(count), JsString(name), JsString(description), JsString(creation_time)) =>
          new FacebookServer.Album(id, count.toInt, name, description, creation_time)
        case _ =>
          throw new DeserializationException("Invalid album object")
      }
    }
  }
}