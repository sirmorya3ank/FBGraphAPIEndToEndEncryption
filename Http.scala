import java.net.InetAddress
import scala.collection.mutable.ArrayBuffer
import scala.concurrent.duration.DurationInt
import java.io.FileOutputStream
import com.typesafe.config.ConfigFactory
import java.util.Calendar
import akka.actor.Actor
import akka.actor.ActorSelection
import akka.actor.ActorSystem
import akka.actor.ActorRef
import akka.actor.Props
import akka.actor.actorRef2Scala
import akka.io.IO
import akka.pattern.ask
import akka.util.Timeout
import akka.util.Timeout.durationToTimeout
import spray.can.Http
import spray.http.ContentTypes
import spray.http.HttpEntity
import spray.http.HttpEntity.apply
import spray.http.HttpMethods.GET
import spray.http.HttpMethods.POST
import spray.http.HttpRequest
import spray.http.HttpResponse
import spray.http.StatusCode.int2StatusCode
import spray.http.Uri
import spray.http.HttpData
import spray.json.pimpAny
import spray.json.pimpString

object HttpServer extends JsonFormats{
	
	def main(args: Array[String]) {
    
    if (args.length < 1) {
      println("One Argument. Private IP Address")
      System.exit(1)
    } 
    else if (args.length == 2) {
      	implicit val system = ActorSystem("HTTPServer", ConfigFactory.load(ConfigFactory.parseString("""{ "akka" : { "actor" : { "provider" : "akka.remote.RemoteActorRefProvider" }, "remote" : { "enabled-transports" : [ "akka.remote.netty.tcp" ], "netty" : { "tcp" : { "port" : 11000 , "maximum-frame-size" : 12800000b } } } } } """)))
		var privateIp = args(0)
      	val server = system.actorSelection("akka.tcp://FacebookServer@" + privateIp + ":12000/user/Watcher/Router")
      	val ipAddress = InetAddress.getLocalHost.getHostAddress()
      	implicit val timeout: Timeout = 10.second // for the actor 'asks'
	
	for (i <- 0 to args(1).toInt - 1) {
       		 // the handler actor replies to incoming HttpRequests
        	val handler = system.actorOf(Props(new HttpService(server)), name = "handler" + i)
        	IO(Http) ? Http.Bind(handler, interface = ipAddress, port = 8080 + i * 4)
      }

      	//val handler = system.actorOf(Props(new HttpService(server)), name = "handler")
      	//IO(Http) ? Http.Bind(handler, interface = ipAddress, port = 8080)
      }
  }

  class HttpService(server: ActorSelection) extends Actor {
    implicit val timeout: Timeout = 5.second // for the actor 'asks'
    import context.dispatcher // ExecutionContext for the futures and scheduler

    def receive = {
      // when a new connection comes in we register ourselves as the connection handler
      case _: Http.Connected => sender ! Http.Register(self)

      case HttpRequest(GET, Uri.Path("/"), _, _, _) =>
        val body = HttpEntity(ContentTypes.`application/json`, "OK")
        sender ! HttpResponse(entity = body)

      

      case HttpRequest(POST, Uri.Path(path), _, entity: HttpEntity.NonEmpty, _) =>
        var parameters:Array[String] = path.split("/")
        var id: String = parameters(1)
        if(id startsWith "U")
          userPostActions(parameters, entity, server, sender)
        else if(id startsWith "PG")
          pagePostActions(parameters, entity, server, sender)
        else if(id startsWith "A")
          albumPostActions(parameters, entity, server, sender)
        else if(id startsWith "PH")
          photoPostActions(parameters, entity, server, sender)
        else 
          tempPostActions(parameters, entity, server, sender)

      case HttpRequest(POST, Uri.Path(path), _, _, _) =>
        var new_path: String = path.replace("?", "/")
        var parameters:Array[String] = new_path.split("/")
        var id: String = parameters(1)
        if(id startsWith "PH")
          photoPostActions(parameters, null, server, sender)

      case HttpRequest(GET, Uri.Path(path), _, entity: HttpEntity.NonEmpty, _) =>
        var parameters:Array[String] = path.split("/")
        var id: String = parameters(1)
        if(id startsWith "PG")
          pageGetActions(parameters, entity, server, sender)
        else if(id startsWith "PH")
          photoGetActions(parameters, server, sender, entity)
        else if(id startsWith "U")
          userGetActions(parameters, server, sender, entity)
        else if(id startsWith "A")
          albumGetActions(parameters, server, sender, entity)

      case _: HttpRequest => sender ! HttpResponse(status = 404, entity = "Unknown!")

    }

    def tempPostActions(parameters: Array[String], entity: HttpEntity, server: ActorSelection, client: ActorRef) {
      var post = entity.data.toByteString
      println(post)
    }

    /////////////////////////////// Post Actions ///////////////////////////////////

    def userPostActions(parameters: Array[String], entity: HttpEntity, server:ActorSelection, client: ActorRef) {
      var numberOfParams:Int = parameters.length
      var mode:String = null
      if(numberOfParams == 2)
        mode = "createProfile"
      else {
        parameters(2) match {
          case "feed" =>
            mode = "publishPost"
          case "photos" =>
            mode = "createPhoto"
          case "albums" =>
            mode = "createAlbum"
          case "saveView" =>
            mode = "addView"
          case "initiate" =>
            mode = "initiate"
        }
      }
      userActions(mode, server, client, parameters, entity)
    }

    def pagePostActions(parameters: Array[String], entity: HttpEntity, server: ActorSelection, client: ActorRef) {
      var mode: String = null
      parameters(2) match {
        case "feed" =>
          mode = "publishPost"
        case "photos" =>
          mode = "createPhoto"
        case "albums" =>
          mode = "createAlbum"
      }
      actions(mode, server, client, parameters, entity)
    }

    def photoPostActions(parameters: Array[String], entity:HttpEntity, server: ActorSelection, client: ActorRef) {
      var mode: String = null
      if(parameters(2).equals("likes"))
        mode = "likePhoto"
      photoActions(mode, server, client, parameters, entity)
    }

    def albumPostActions(parameters: Array[String], entity: HttpEntity, server: ActorSelection, client: ActorRef) {
      var mode: String = null
      parameters(2) match {
        case "photos" =>
          mode = "createPhoto"
        case "file-upload" =>
          mode = "uploadPhoto"
      }
      actions(mode, server, client, parameters, entity)
    }

    ////////////////////////////////////////////////////////////////////////////////

    /////////////////////////////// Get Actions ////////////////////////////////////
    
    def userGetActions(parameters: Array[String], server: ActorSelection, client: ActorRef, entity: HttpEntity) {
      var numberOfParams: Int = parameters.length
      var mode: String = null
      if(numberOfParams == 2)
        mode = "getProfile"
      else if(parameters(2).equals("friends"))
        mode = "getFriends"
      else if(parameters(2).equals("feed") || parameters(2).equals("posts") || parameters(2).equals("tagged"))
        mode = "getTimeline"
      else if(parameters(2).equals("likes"))
        mode = "getLikes"
      else if(parameters(2).equals("audience")) 
        mode = "getAudience"
      else if(parameters(2).equals("photos"))
        mode = "getPhotos"
      else if(parameters(2).equals("albums"))
        mode = "getAlbums"
      
      userActions(mode, server, client, parameters, entity)
    }

    def pageGetActions(parameters: Array[String], entity: HttpEntity, server: ActorSelection, client: ActorRef) {
      var mode: String  = null
      parameters(2) match {
        case "feed" =>
          mode = "getTimeline"
        case "posts" =>
          mode = "getTimeline"
        case "tagged" =>
          mode = "getTimeline"
      }
      actions(mode, server, client, parameters, entity)
    }

    def photoGetActions(parameters: Array[String], server: ActorSelection, client: ActorRef, entity: HttpEntity) {
      var mode: String = null
      if(parameters.length == 2)
        mode = "getPhoto"
      else {
        parameters(2) match {
          case "likes" =>
            mode = "getLikes"
          case "tags" =>
            mode = "getTags"
        }
      }
      photoActions(mode, server, client, parameters, entity)
    }

    def albumGetActions(parameters: Array[String], server: ActorSelection, client: ActorRef, entity: HttpEntity) {
      var mode: String = null
      if(parameters.length == 2)
        mode = "getAlbum"
      else {
        parameters(2) match {
          case "photos" =>
            mode = "getPhotos"

        }
      }
      albumActions(mode, server, client, parameters, entity)
    }
    ////////////////////////////////////////////////////////////////////////////////

    /////////////////////////////// User Actions ///////////////////////////////////
    def userActions(mode:String, server:ActorSelection, client: ActorRef, parameters: Array[String], entity:HttpEntity) {
      
      mode match {

        case "initiate" =>
          var id = parameters(1)
          var clientPublicKey: String = entity.data.asString
          var result = server ? FacebookServer.Server.initiateClient(id, clientPublicKey)
          result onSuccess {
            case result: String =>
              val body = HttpEntity(ContentTypes.`text/plain`, result)
              client ! HttpResponse(entity = body)
          }
        
        case "createProfile" =>
          var entityMsg: Array[String] = entity.data.asString.split("\\$\\$\\$\\$\\$\\$\\$")
          var token: String = entityMsg(0)+"########"+entityMsg(1)
          var profile: String = entityMsg(2)
          val result = server ? FacebookServer.Server.createProfile(token, profile)
          result onSuccess {
            case result: String =>
              val body = HttpEntity(ContentTypes.`text/plain`, result)
              client ! HttpResponse(entity = body)
          }

        case "getAudience" =>
          var entityMsg: Array[String] = entity.data.asString.split("\\$\\$\\$\\$\\$\\$\\$")
          var token: String = entityMsg(0)+"########"+entityMsg(1)
          var id = parameters(1)
          val result = (server ? FacebookServer.Server.getFriendKeys(token, id)).mapTo[List[String]]
          result onSuccess {
            case result: List[String] =>
              val body = HttpEntity(ContentTypes.`application/json`, result.toJson.toString)
              client ! HttpResponse(entity = body)
          }

        case "addView" =>
          var entityMsg: Array[String] = entity.data.asString.split("\\$\\$\\$\\$\\$\\$\\$")
          var token: String = entityMsg(0)+"########"+entityMsg(1)
          var id = parameters(1)
          var entityMessages: Array[String] = entityMsg(2).parseJson.convertTo[Array[String]]
          val result = server ? FacebookServer.Server.addView(token, id, entityMessages)
          result onSuccess {
            case result: String =>
              val body = HttpEntity(ContentTypes.`text/plain`, result)
              client ! HttpResponse(entity = body)
          }

        case "getFriends" =>
          var entityMsg: Array[String] = entity.data.asString.split("\\$\\$\\$\\$\\$\\$\\$")
          var token: String = entityMsg(0)+"########"+entityMsg(1)
          var friendId: String = null
          val id = parameters(1)
          if(parameters.length == 4)
            friendId = parameters(3)
          val result = (server ? FacebookServer.Server.getFriends(token, id, friendId)).mapTo[List[String]]
          var body: HttpEntity = null
          result onSuccess {
            case result: List[String] =>
              if(result.isEmpty) {
                if(friendId == null)
                  body = HttpEntity(ContentTypes.`text/plain`, "No friends present")
                else
                  body = HttpEntity(ContentTypes.`text/plain`, parameters(3)+" is not a friend")
              }
              else
                body = HttpEntity(ContentTypes.`application/json`, result.toJson.toString)
              client ! HttpResponse(entity = body)
          }
          result onFailure {
            case _ =>
              println("Some error occurred while getting the friend info from server")
          }

         case "publishPost" =>
           actions(mode, server, client, parameters, entity)

         case "getTimeline" =>
           actions(mode, server, client, parameters, entity)

        case "getLikes" =>
          var entityMsg: Array[String] = entity.data.asString.split("\\$\\$\\$\\$\\$\\$\\$")
          var token: String = entityMsg(0)+"########"+entityMsg(1)
          var userId: String = parameters(1)
          val result = (server ? FacebookServer.Server.getLikes(token, userId)).mapTo[List[List[String]]]
          var body: HttpEntity = null
          result onSuccess {
            case result: List[List[String]] =>
              if(result.isEmpty) {
                body = HttpEntity(ContentTypes.`text/plain`, "No pages liked")
              }
              else
                body = HttpEntity(ContentTypes.`application/json`, result.toJson.toString)
              client ! HttpResponse(entity = body)
          }

        case "getAlbums" =>
          var entityMsg: Array[String] = entity.data.asString.split("\\$\\$\\$\\$\\$\\$\\$")
          var token: String = entityMsg(0)+"########"+entityMsg(1)
          var target: String = parameters(1)
          val result = (server ? FacebookServer.Server.getAlbums(token, target)).mapTo[List[String]]
          var body: HttpEntity = null
          result onSuccess {
            case result: List[String] =>
              if(result.isEmpty) 
                body = HttpEntity(ContentTypes.`text/plain`, "No albums to show")
              else
                body = HttpEntity(ContentTypes.`application/json`, result.toJson.toString)
              client ! HttpResponse(entity=body)
          }
        
         case "createPhoto" =>
           actions(mode, server, client, parameters, entity)

        case "createAlbum" =>
          actions(mode, server, client, parameters, entity)

          case "getPhotos" =>
            actions(mode, server, client, parameters, entity)
        }
    }
    ////////////////////////////////////////////////////////////////////////////////

    /////////////////////////////// Photo Actions //////////////////////////////////
    def photoActions(mode:String, server:ActorSelection, client: ActorRef, parameters: Array[String], entity:HttpEntity) {
       mode match {

        case "getPhoto" =>
          var entityMsg: Array[String] = entity.data.asString.split("\\$\\$\\$\\$\\$\\$\\$")
          var token: String = entityMsg(0)+"########"+entityMsg(1)
          var photoId: String = parameters(1)
          val result = (server ? FacebookServer.Server.getPhoto(token, photoId)).mapTo[List[String]]
          var body: HttpEntity = null
          result onSuccess {
            case result: List[String] =>
              if(result.isEmpty)
                body = HttpEntity(ContentTypes.`text/plain`, "Photo not present")
              else
                body = HttpEntity(ContentTypes.`application/json`, result.toJson.toString)
              client ! HttpResponse(entity = body)
          }

        case "getLikes" =>
          var entityMsg: Array[String] = entity.data.asString.split("\\$\\$\\$\\$\\$\\$\\$")
          var token: String = entityMsg(0)+"########"+entityMsg(1)
          var photoId: String = parameters(1)
          val result = (server ? FacebookServer.Server.getPhotoLikes(token, photoId)).mapTo[List[String]]
          var body: HttpEntity = null
          result onSuccess {
            case result: List[String] =>
              if(result.isEmpty)
                body = HttpEntity(ContentTypes.`text/plain`, "No likes")
              else
                body = HttpEntity(ContentTypes.`application/json`, result.toJson.toString)
              client ! HttpResponse(entity = body)

          }

        case "likePhoto" =>
          var entityMsg: Array[String] = entity.data.asString.split("\\$\\$\\$\\$\\$\\$\\$")
          var token: String = entityMsg(0)+"########"+entityMsg(1)
          var photo_id: String = parameters(1)
          val result = server ? FacebookServer.Server.likeObject(token, photo_id)
          result onSuccess {
            case result: String =>
              val body = HttpEntity(ContentTypes.`text/plain`, result)
              client ! HttpResponse(entity = body)
          }
       }
    }
    ////////////////////////////////////////////////////////////////////////////////

    /////////////////////////////// Album Actions //////////////////////////////////

    def albumActions(mode: String, server: ActorSelection, client: ActorRef, parameters: Array[String], entity:HttpEntity) {
      mode match {
        case "getAlbum" =>
          var entityMsg: Array[String] = entity.data.asString.split("\\$\\$\\$\\$\\$\\$\\$")
          var token: String = entityMsg(0)+"########"+entityMsg(1)
          var album_id: String = parameters(1)
          val result = (server ? FacebookServer.Server.getAlbum(token, album_id)).mapTo[List[String]]
          var body: HttpEntity = null
          result onSuccess {
            case result: List[String] =>
              if(result.isEmpty)
                body = HttpEntity(ContentTypes.`text/plain`, "Album not present")
              else
                body = HttpEntity(ContentTypes.`application/json`, result.toJson.toString)
              client ! HttpResponse(entity = body)
          }

        case "getPhotos" =>
          actions(mode, server, client, parameters, entity)
      }
    }
    ////////////////////////////////////////////////////////////////////////////////

    /////////////////////////////// Common Actions /////////////////////////////////

    def actions(mode:String, server:ActorSelection, client: ActorRef, parameters: Array[String], entity:HttpEntity) {
      mode match {
        
        case "publishPost" =>
          var entityMsg: Array[String] = entity.data.asString.split("\\$\\$\\$\\$\\$\\$\\$")
          var token: String = entityMsg(0)+"########"+entityMsg(1)
          var message: Array[String] = entityMsg(2).parseJson.convertTo[Array[String]]
          var post: String = message(0)
          var audience: ArrayBuffer[String] = ArrayBuffer.empty
          var audienceLength: Int = message.length-1
          for(index <- 1 to audienceLength)
            audience += message(index)
           val to_id = parameters(1)
          val result = server ? FacebookServer.Server.publishPost(token, post, to_id, audience)
          result onSuccess {
            case result: String =>
              val body: HttpEntity = HttpEntity(ContentTypes.`text/plain`, result.toString)
              client ! HttpResponse(entity = body)
         }

        case "getTimeline" =>
          var entityMsg: Array[String] = entity.data.asString.split("\\$\\$\\$\\$\\$\\$\\$")
          var token: String = entityMsg(0)+"########"+entityMsg(1)
          var userId: String = parameters(1)
          val result = (server ? FacebookServer.Server.getFeed(token, userId)).mapTo[List[String]]
          var body: HttpEntity = null
          result onSuccess {
            case result: List[String] =>
              if(result.isEmpty)
                body = HttpEntity(ContentTypes.`text/plain`, "No posts to show")
              else
                body = HttpEntity(ContentTypes.`application/json`, result.toJson.toString)
              client ! HttpResponse(entity = body)
            }

        case "createPhoto" =>
          var entityMsg: Array[String] = entity.data.asString.split("\\$\\$\\$\\$\\$\\$\\$")
          var token: String = entityMsg(0)+"########"+entityMsg(1)
          var message: Array[String] = entityMsg(2).parseJson.convertTo[Array[String]]
          var photo: String = message(0)
          var audience: ArrayBuffer[String] = ArrayBuffer.empty
          var audienceLength: Int = message.length-1
          for(index <- 1 to audienceLength)
            audience += message(index)
          val to_id: String = parameters(1)
          var from_id: String = parameters(1)
          val result = server ? FacebookServer.Server.addPhoto(token, photo, to_id, audience)
          result onSuccess {
            case result: String =>
              val body: HttpEntity = HttpEntity(ContentTypes.`text/plain`, result.toString)
              client ! HttpResponse(entity = body)
          }
       

        case "createAlbum" =>
          var entityMsg: Array[String] = entity.data.asString.split("\\$\\$\\$\\$\\$\\$\\$")
          var token: String = entityMsg(0)+"########"+entityMsg(1)
          var message: Array[String] = entityMsg(2).parseJson.convertTo[Array[String]]
          var album: String = message(0)
          var audience: ArrayBuffer[String] = ArrayBuffer.empty
          var audienceLength: Int = message.length-1
          for(index <- 1 to audienceLength)
            audience += message(index)
          val source: String = parameters(1)
          val result = server ? FacebookServer.Server.addAlbum(token, album, audience)
          var body: HttpEntity = null
          result onSuccess {
            case result: String =>
              body = HttpEntity(ContentTypes.`text/plain`, result.toString)
              client ! HttpResponse(entity = body)
          }

        case "getPhotos" =>
          var container_id: String = parameters(1)
          var entityMsg: Array[String] = entity.data.asString.split("\\$\\$\\$\\$\\$\\$\\$")
          var token: String = entityMsg(0)+"########"+entityMsg(1)
          val result = (server ? FacebookServer.Server.getPhotos(token, container_id)).mapTo[List[String]]
          var body: HttpEntity = null
          result onSuccess {
            case result: List[String] =>
              if(result.isEmpty)
                body = HttpEntity(ContentTypes.`text/plain`, "No photo to show")
              else
                body = HttpEntity(ContentTypes.`application/json`, result.toJson.toString)
            client ! HttpResponse(entity = body)
          }

        case _ => 
          println("Invalid request")
      }
    }

    ////////////////////////////////////////////////////////////////////////////////

    

    

    

    

    
  }
}
