import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.ConcurrentLinkedQueue
import java.util.concurrent.CopyOnWriteArrayList
import java.util.concurrent.atomic.AtomicInteger
import java.util.Calendar
import scala.collection.mutable.ArrayBuffer
import scala.concurrent.duration.DurationInt

import com.typesafe.config.ConfigFactory
import spray.json._
import spray.json.pimpAny
import spray.json.pimpString
import akka.actor.Actor
import akka.actor.ActorRef
import akka.actor.ActorSystem
import akka.actor.Props
import akka.actor.Terminated
import akka.actor.actorRef2Scala
import akka.event.LoggingReceive
import akka.routing.SmallestMailboxPool
import scala.collection.mutable.ListBuffer
object FacebookServer extends JsonFormats {
	class UserProfile(input_id: String, input_name: String, input_gender: String, input_dob: String, input_relStatus: String) extends java.io.Serializable {
    var id: String = input_id
    var name: String  = input_name
    var gender: String = input_gender
    var dob: String =  input_dob
    var relStatus: String = input_relStatus
  }

  class Post(input_id: String, input_message: String, input_from: String, input_to: String, input_taggers: ArrayBuffer[String]) extends java.io.Serializable {
    var id: String = input_id
    var message: String = input_message
    var from: String = input_from
    var to: String = input_to
    var tags: ArrayBuffer[String] = input_taggers
  }

  class Photo(input_id: String, input_album: String, input_creation_time: String, input_from: String, input_name: String, input_link: String) extends java.io.Serializable {
    var id: String = input_id
    var album: String = input_album
    var creationTime: String = input_creation_time
    var from: String = input_from
    var name: String = input_name
    var link: String = input_link
  }

  class Album(input_id: String, input_count: Int, input_name: String, input_description: String, input_creation_time: String) extends java.io.Serializable {
    var id: String = input_id
    var count: Int = input_count
    var name: String = input_name
    var description: String = input_description
    var creation_time: String = input_creation_time
  }

  var keypair = RSA.generateKey
  var publicKey = RSA.encodePublicKey(keypair.getPublic)
  var clientTokens: ConcurrentHashMap[String, String] = new ConcurrentHashMap()
	var friendListMap: ConcurrentHashMap[String, ArrayBuffer[String]] = new ConcurrentHashMap()
	var publicKeys: ConcurrentHashMap[String, String] = new ConcurrentHashMap();
	var objects: ConcurrentHashMap[String, String] = new ConcurrentHashMap();
  var posts: ConcurrentHashMap[String, String] = new ConcurrentHashMap();
  var photos: ConcurrentHashMap[String, String] = new ConcurrentHashMap();
  var albums: ConcurrentHashMap[String, String] = new ConcurrentHashMap();
  var photoContainer: ConcurrentHashMap[String, ArrayBuffer[String]] = new ConcurrentHashMap();
	var views: ConcurrentHashMap[String, ConcurrentHashMap[String, String]] = new ConcurrentHashMap();
  var postContainer: ConcurrentHashMap[String, ArrayBuffer[String]] = new ConcurrentHashMap();
  var userPostMap: ConcurrentHashMap[String, ArrayBuffer[String]] = new ConcurrentHashMap();
  var userPageMap: ConcurrentHashMap[String, ArrayBuffer[String]] = new ConcurrentHashMap();
  var pageUserMap: ConcurrentHashMap[String, ArrayBuffer[String]] = new ConcurrentHashMap();
  var userAlbumMap: ConcurrentHashMap[String, ArrayBuffer[String]] = new ConcurrentHashMap();
  var likes: ConcurrentHashMap[String, ArrayBuffer[String]] = new ConcurrentHashMap();

	var serverWatcherRef: ActorRef = null
    var noOfUsers : Int = 0

	def main(args: Array[String]) {
		noOfUsers = args(0).toInt
    	val system = ActorSystem("FacebookServer", ConfigFactory.load(ConfigFactory.parseString("""{ "akka" : { "actor" : { "provider" : "akka.remote.RemoteActorRefProvider" }, "remote" : { "enabled-transports" : [ "akka.remote.netty.tcp" ], "netty" : { "tcp" : { "port" : 12000 , "maximum-frame-size" : 12800000b } } } } } """)))
		val watcher = system.actorOf(Props(new Watcher()), name = "Watcher")
		serverWatcherRef = watcher
		watcher ! FacebookServer.Watcher.Init()
        //watcher ! Project4Server.Watcher.Init(args(0).toInt)
  	}

  	object Watcher {
    	case class Init()
    	case class profileCreated()
    	case object Time
  	}

  	class Watcher extends Actor {
		import Watcher._
    import context._
    var usersCreated : Int = 0
    // Start a router with 30 Actors in the Server.
    var cores = (Runtime.getRuntime().availableProcessors() * 1.5).toInt
    val router = context.actorOf(Props[Server].withRouter(SmallestMailboxPool(cores)), name = "Router")
    // Watch the router. It calls the terminate sequence when router is terminated.
    //context.watch(router)

    final def receive = LoggingReceive {
      case Init() =>
        //Initialize(noOfUsers)
        System.gc()
		
      case profileCreated() =>
	println("Watcher is called on account creation")
        usersCreated = usersCreated + 1
	if(usersCreated == noOfUsers){
                println("Creating Friend List")
		var k : Int = 3
		for(i <- 0 to noOfUsers-1){
      var userId: String = "U"+i
      for(j <- 1 to k) {
        var friendId: String = "U"+((i + j)%noOfUsers)
        addFriend(userId, friendId)
        addFriend(friendId, userId)
      }
			// var nodeIds = new ArrayBuffer[String]()
			// for(j <- 1 to k){
			// 	nodeIds += "U"+((i + j)%noOfUsers)
			// }
			// friendListMap.put("U"+i,nodeIds)
		}
	}

    case _ => println("FAILED HERE")
    }
  }

  

  def addFriend(user1: String, user2: String) {
    var friends1: ArrayBuffer[String] = ArrayBuffer.empty
    if(friendListMap.get(user1)!=null)
      friends1 = friendListMap.get(user1);
    friends1 += user2
    friendListMap.put(user1, friends1)
  }

  object Server {
    case class initiateClient(client_id: String, client_pubkey: String)
    case class createProfile(token: String, profile: String)
    case class getFriendKeys(token: String, userId: String)
    case class addView(token: String, objectId: String, audience: Array[String])
    case class getFriends(token: String, user1: String, user2: String)
    case class publishPost(token: String, post: String, target: String, audience: ArrayBuffer[String])
    case class getFeed(token: String, target: String)
    case class getLikes(token: String, userId: String)
    case class addPhoto(token: String, photo: String, target: String, audience: ArrayBuffer[String])
    case class getPhoto(token: String, photoId: String)
    case class addAlbum(token: String, album: String, audience: ArrayBuffer[String])
    case class getAlbum(token: String, albumId: String)
    case class getPhotos(token: String, container: String)
    case class getAlbums(token: String, user: String)
    case class likeObject(token: String, objectId: String)
    case class getPhotoLikes(token: String, objectId: String)
	}

	class Server extends Actor {
    	import Server._
    	import context._
      

    // Receive block for the Server.
    	final def receive = LoggingReceive {

        case initiateClient(client_id, client_pubkey) =>
          var token = RSA.generateAESKey
          clientTokens.put(token, client_id)
          publicKeys.put(client_id, client_pubkey)
          var result: String = publicKey + "########" + RSA.encrypt(token, RSA.decodePublicKey(client_pubkey))
          sender ! result

  			case createProfile(token, profile) =>
          var id: String = authenticateUser(token)
          if(id==null)
            sendErrorMessage(sender)
  				objects.put(id, profile)
  				createPages(id)
  				var successString = "User "+id+" created successfully"
        	serverWatcherRef ! Watcher.profileCreated()
        	sender ! successString

  			case getFriendKeys(token, userId) =>
          var id: String = authenticateUser(token)
          if(id==null)
            sendErrorMessage(sender)
  				var friends: ArrayBuffer[String] = friendListMap.get(userId)
  		    var list: ArrayBuffer[String] = new ArrayBuffer()
  				for(friend <- friends) {
  					list += friend+"########"+publicKeys.get(friend)
  				}
  				sender ! list.toList

        case addView(token, objectId, audience) =>
          var id: String = authenticateUser(token)
          if(id==null)
            sendErrorMessage(sender)
          var map: ConcurrentHashMap[String, String] = new ConcurrentHashMap()
          var str: String = ""
          for(user <- audience) {
            var keyValue: Array[String] = user.split("########")
            map.put(keyValue(0), keyValue(1))
            str = str+" "+keyValue(0)
          }
          views.put(objectId, map)
          var successString: String = "View added for "+objectId+" successfully"
          sender ! successString

        case getFriends(token, user1, user2) =>
          var id: String = authenticateUser(token)
          if(id==null)
            sendErrorMessage(sender)
          var result: ArrayBuffer[String] = ArrayBuffer.empty
          var friendIds: ArrayBuffer[String] = friendListMap.get(user1)
          var friendInfo: String = ""
          if(user2 != null) {
            friendInfo = objects.get(user2) + "########" + views.get(user2).get(user1)
            result += friendInfo
          }
          else {
            for(friendId <- friendIds) {
              friendInfo = objects.get(friendId) + "########" + views.get(friendId).get(user1)
              result += friendInfo
            }
          }
          sender ! result.toList

        case publishPost(token, post, target, audience) =>
          var id: String = authenticateUser(token)
          if(id==null)
            sendErrorMessage(sender)
          var newId: Int = posts.size
          while(posts.get(newId)!=null)
            newId += 1
          var objectId: String = "P"+newId
          posts.put(objectId, post)
          addElementToMap(postContainer, target, objectId)
          addObjectToUserMap(userPostMap, objectId, audience)
          addViewImpl(objectId, audience)
          sender ! objectId

        case getFeed(token, target) =>
          var id: String = authenticateUser(token)
          if(id==null)
            sendErrorMessage(sender)
          var feed: ArrayBuffer[String] = ArrayBuffer.empty
          var postIds: ArrayBuffer[String] = ArrayBuffer.empty
          if(target.startsWith("U")) 
            postIds ++= userPostMap.get(target)
          else if(target.startsWith("PG"))
            postIds ++= postContainer.get(target)
          for(postId <- postIds) { 
            var postInfo: String = posts.get(postId) + "########" + views.get(postId).get(id)
            feed += postInfo
          }
          sender ! feed.toList

        case getLikes(token, userId) =>
          var id: String = authenticateUser(token)
          if(id==null)
            sendErrorMessage(sender)
          var publicKey: String = publicKeys.get(userId)
          var pages: ArrayBuffer[String] = userPageMap.get(userId)
          var result: ArrayBuffer[List[String]] = ArrayBuffer.empty
          for(page <- pages) {
            var list: ArrayBuffer[String] = ArrayBuffer.empty
            list += encryptMessage(page, publicKey)
            var users: ArrayBuffer[String] = pageUserMap.get(page)
            for(user <- users) 
              list += (user + "########" + publicKeys.get(user))
            result += (list.toList)
          }
          sender ! result.toList

        case addPhoto(token, photo, target, audience) =>
          var source: String = authenticateUser(token)
          if(source==null)
            sendErrorMessage(sender)
          var newId: Int = photos.size
          while(photos.get(newId) != null)
            newId += 1
          var photoId: String = "PH"+newId
          photos.put(photoId, photo)
          addElementToMap(photoContainer, target, photoId)
          if(!source.equals(target))
            addElementToMap(photoContainer, source, photoId)
          addViewImpl(photoId, audience)
          sender ! photoId

        case getPhoto(token, photoId) =>
          var from: String = authenticateUser(token)
          if(from==null)
            sendErrorMessage(sender)
          var photoIds: ArrayBuffer[String] = ArrayBuffer.empty
          photoIds += photoId
          var photo: List[String] = getObjects(photos, photoIds, from)
          sender ! photo

        case addAlbum(token, album, audience) =>
          var source: String = authenticateUser(token)
          if(source==null)
            sendErrorMessage(sender)
          var albumId: String = addNewObject(albums, album)
          addElementToMap(userAlbumMap, source, albumId)
          addViewImpl(albumId, audience)
          sender ! albumId

        case getAlbum(token, albumId) =>
          var source: String = authenticateUser(token)
          if(source==null)
            sendErrorMessage(sender)
          var albumIds: ArrayBuffer[String] = ArrayBuffer.empty
          albumIds += albumId
          var album: List[String] = getObjects(albums, albumIds, source)
          sender ! album

        case getPhotos(token, container) =>
          var source: String = authenticateUser(token)
          if(source==null)
            sendErrorMessage(sender)
          var photoIds: ArrayBuffer[String] = photoContainer.get(container)
          var photo: List[String] = getObjects(photos, photoIds, source)
          sender ! photo

        case getAlbums(token, user) =>
          var id: String = authenticateUser(token)
          if(id==null)
            sendErrorMessage(sender)
          var albums: ArrayBuffer[String] = ArrayBuffer.empty
          albums ++= userAlbumMap.get(user)
          sender ! albums.toList

        case likeObject(token, objectId) =>
          var user: String = authenticateUser(token)
          if(user==null)
            sendErrorMessage(sender)
          var str: String = views.get(objectId).get(user)
          var result: String = null
          if(str == null)
            result = user + "cannot like " + objectId
          else {
            addElementToMap(likes, objectId, user)
            result = user + " likes " + objectId
          }
          sender ! result

        case getPhotoLikes(token, objectId) =>
          var user: String = authenticateUser(token)
          if(user==null)
            sendErrorMessage(sender)
          var str: String = views.get(objectId).get(user)
          var result: ArrayBuffer[String] = ArrayBuffer.empty
          if(str == null) 
            result += (user + "cannot view " + objectId)
          else {
            result += likes.get(objectId).length.toString
            result ++= likes.get(objectId)
          }
          sender ! result.toList
  		}

       
  	}

    def authenticateUser(authString: String): String = {
      var authMessages: Array[String] = authString.split("########")
      var from: String = authMessages(0)
      var token: String = RSA.decrypt(authMessages(1), keypair.getPrivate)
      var id: String = clientTokens.get(token)
      if((id==null)||(id.equals(from)))
        return from
      else
        return null
      }

      def sendErrorMessage(sender: ActorRef): Unit = {
        var message: String = "You dont seem to be what you are"
        sender ! message
      }     

    def getObjects(map: ConcurrentHashMap[String, String], objectIds: ArrayBuffer[String], user: String): List[String] = {
      var content: String = ""
      var contents: ArrayBuffer[String] = ArrayBuffer.empty
      for(objectId <- objectIds) {
        content = views.get(objectId).get(user)
        if(content != null)
          contents += (map.get(objectId) + "########" + content)
      }
      return contents.toList
    }

    def addNewObject(map: ConcurrentHashMap[String, String], content: String): String = {
      var newId: Int = map.size
      while(map.get(newId) != null)
        newId += 1
      var objectId: String = "A"+newId
      map.put(objectId, content)
      return objectId
    }

    def encryptMessage(message: String, publicKey: String): String = {
      var encMsg: String = RSA.encrypt(message, RSA.decodePublicKey(publicKey))
      return encMsg
    }

    def createPages(userId: String): Unit = {
      var id: String = userId.substring(1)
      var pageId: String = "PG" + id
      addElementToMap(userPageMap, userId, pageId)
      addElementToMap(pageUserMap, pageId, userId)
      pageId = "PG"+((id.toInt + 1)%noOfUsers)
      addElementToMap(userPageMap, userId, pageId)
      addElementToMap(pageUserMap, pageId, userId)
    }

    def addElementToMap(map: ConcurrentHashMap[String, ArrayBuffer[String]], key: String, element: String): Unit = {
      var values: ArrayBuffer[String] = map.get(key)
      if(values == null)
        values = ArrayBuffer.empty
      values += element
      map.put(key, values) 
    }

    def addObjectToUserMap(map: ConcurrentHashMap[String, ArrayBuffer[String]], objectId: String, audience: ArrayBuffer[String]): Unit = {
      for(user <- audience) {
        var userId: String = user.split("########")(0)
        var objects: ArrayBuffer[String] = map.get(userId)
        if(objects==null)
          objects = ArrayBuffer.empty
        objects += objectId
        map.put(userId, objects)
      }
    }

    def addViewImpl(objectId: String, audience: ArrayBuffer[String]): Unit = {
      var map: ConcurrentHashMap[String, String] = views.get(objectId)
      if(map == null)
        map = new ConcurrentHashMap()
      var str: String = ""
      for(user <- audience) {
        var keyValue: Array[String] = user.split("########")
        map.put(keyValue(0), keyValue(1))
      }
      views.put(objectId, map)
    }
}